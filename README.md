## Gift A School Kit
Gift A School Kit is a web3 project that provides underpriviledged school kids in Africa access to basic educational materials.

## Help School Kids Achieve Their Dreams
Donate Today - https://solana-school-kit.vercel.app/

## Getting Started

First, clone the repo, `yarn` and run the development server:

```bash
npm run dev
# or
yarn dev
```
