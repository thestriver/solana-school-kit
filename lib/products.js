export const products = [
  {
    id: 'school-kit',
    unitName: 'school-kit', // shows after the price, eg. 0.05 SOL/box
    priceSol: 0.05,
    priceUsd: 20,
  }
]
