import CardPayment from "./CardPayment";
import Dreams from "./Dreams";
import SolanaPayment from "./SolanaPayment";

export default function Donation({ reference }){
    return (

        <section className="w-full bg-gray-50 pt-7 pb-7 md:pt-10 md:pb-24">
        <div className="box-border flex flex-col items-center content-center px-8 mx-auto leading-6 text-black border-0 border-gray-300 border-solid md:flex-row max-w-9xl lg:px-20">

           <Dreams />
    
            <div ref={reference} className="box-border order-first w-full text-black border-solid md:w-1/2 md:pl-10 md:order-none">
                <ul className="p-0 m-0 leading-6 border-0 border-gray-300">
                    <li className="box-border relative py-1 pl-0 text-left text-black-800 border-solid lg:text-4xl md:text-2xl">
                        <span className="inline-flex items-center justify-center w-16 h-16 mr-2 text-white bg-indigo-500 rounded-full">
                            <span className="text-2xl font-bold">$20</span>
                        </span> Essentials
                    </li>
                </ul>
                <p className="text-gray-800 leading-loose px-1 md:px-20">
                    An Essentials School Kit pack includes a school backpack, a set of notebooks and other essential educational materials for a school kid.
                </p>
                <p className="text-gray-800 leading-loose px-1 md:px-20">
                    We&apos;ve provided two main ways for donating. You have the option of paying with a credit card or paying with Solana.
                </p>

                <CardPayment />
                
                <SolanaPayment />
            </div>
        </div>
        
    </section>

    )
}