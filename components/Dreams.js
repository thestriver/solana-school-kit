export default function Dreams(){
    return (
        <div className="box-border w-full text-black border-solid md:w-1/2 md:pl-6 xl:pl-32 my-5 md:my-1 md:pr-20 ">
            <h2 className="mt-4 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-5xl md:text-2xl">
            Help School Kids Achieve Their Dreams
            </h2>
            <p className="mb-2 leading-loose text-gray-800 ">
                There is a huge chance Crypto has had a huge influence on your life. And we&apos;re hoping you can help make it changed others too. </p>
                
            <p className="mb-2 leading-loose text-gray-800 "> There are over <span className="font-bold" >200 million out of school children globally </span>
                and those still in school lack appropriate learning materials to truly achieve their goals.
            </p>
                
            <p className="mb-2 leading-loose text-gray-800 "> Over the past couple of years, I&apos;ve participated in efforts to donate proper school kits to needy school kids especially 
                in places where millions are living in extreme poverty but there&apos;s still so much to be done. 
            </p>
        <p className="mb-2 leading-loose text-gray-800">
                With Gift A School Kit, <b>we can help make a difference</b>. Help these kids fulfil their right to education. Help them achieve their dreams. 
                <b> Gift A School Kit Today.</b>
                
                
            </p>
    </div>
    )
}