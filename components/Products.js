import { useRef } from "react";
import { products } from "../lib/products"
import NumberInput from "./NumberInput";


export default function Products({ submitTarget, enabled }) {
  const formRef = useRef(null);

  return (
    <form method='get' action={submitTarget}>
      <div className='flex flex-col gap-6 py-5'>
        <div className="grid grid-cols-2">
          {products.map(product => {
            return (
              <div className="rounded-md bg-white text-left p-2" key={product.id}>
                <p className="my-4">
                  {/* <span className="mt-4 text-xl font-bold">{product.priceSol} SOL</span> */}
                  <span className="mt-4 text-xl font-bold">${product.priceUsd}</span>
                  {product.unitName && <span className="text-sm text-gray-800"> /{product.unitName}</span>}
                </p>
                <div className="mt-1">
                  <NumberInput name={product.id} formRef={formRef} />
                </div>
              </div>
            )
          })}

        </div>

        <button
          className="items-center px-20 rounded-md py-2 max-w-fit bg-gray-900 text-white hover:scale-105 disabled:opacity-50 disabled:cursor-not-allowed"
          disabled={!enabled}
        >
          Donate
        </button>
      </div>
    </form>
  )
}
