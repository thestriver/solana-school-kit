import React, { useState , useEffect} from 'react';
import { useWallet } from '@solana/wallet-adapter-react'
import { WalletMultiButton } from '@solana/wallet-adapter-react-ui'
import Products from './Products'
import Image from 'next/image'

export default function SolanaPayment(){
    const { publicKey } = useWallet()
    const [ paySolana, setPaySolana ] = useState(false) 
    return (
        <div className='md:px-20 py-5'>
            <div className=" flex bg-black hover:bg-gray-800 text-white font-bold py-3 px-20 w-full lg:w-full xl:w-3/5 justify-center rounded cursor-pointer"
            onClick={() => setPaySolana(true) }
            >
            <span  className="pr-2">Donate with</span> <Image  src='/solana-pay.png' alt="school kids" width={60} height={24}/>
            </div>
            
                                
            {paySolana ?
                <div className='py-5'>
                    <div className="basis-1/4">
                        <WalletMultiButton className='!bg-gray-900 hover:scale-105' />
                    </div>
                    <Products submitTarget='/donate' enabled={publicKey !== null} />
                </div>
            :null}
                
        </div>
    )
}