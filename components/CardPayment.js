import React from 'react';
import { loadStripe } from '@stripe/stripe-js';

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(
  process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY
);
export default function CardPayment() {
  React.useEffect(() => {
    // Check to see if this is a redirect back from Checkout
    const query = new URLSearchParams(window.location.search);
    if (query.get('success')) {
      console.log('Order placed! You will receive an email confirmation.');
    }

    if (query.get('canceled')) {
      console.log('Order canceled -- continue to shop around and checkout when you’re ready.');
    }
  }, []);

  return (
    <form action="/api/checkout_sessions" method="POST" className='md:px-20 py-5'>
      <section>
        <button type="submit" role="link" className=''>
          Donate With Credit Card 💖
        </button>
        <small>powered by <span>Stripe</span></small>
      </section>
      <style jsx>
        {`
          section {
            display: flex;
            flex-direction: column;
            width: 400px;
            border-radius: 6px;
            justify-content: space-between;
          }
          button {
            background: #2E9BFF;
            padding: 10px 36px;
            color: white;
            font-weight: 600;
            cursor: pointer;
            transition: all 0.2s ease;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 4px;
          }
          small {
            text-align: right;
          }
          span {
            color: blue;
          }
          button:hover {
            opacity: 0.9;
          }
          @media only screen and (max-width: 600px) {
            button {
              padding: 10px 26px;
              width: 75%
            }
            small {
              text-align: left;
            }
          }
        `}
      </style>
    </form>
  );
}