import React, { useRef } from 'react';
import Head from 'next/head'
import Hero from '../components/Hero'
import Donation from '../components/Donation'
import Newsletter from '../components/newsletter'
import Footer from '../components/footer'

export default function Home() {
  const scrollToDiv = (ref) => window.scrollTo({ top: ref.current.offsetTop, behavior: 'smooth' });
  const el1 = useRef();
  const el2 = useRef();
  return (
   <div className='overflow-x-hidden;' style={{ overflowX: 'hidden'}} >
     <Head>
          <title>Donate a SchoolKit</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      <Hero reference={el1} click={()=> scrollToDiv(el2)} />
      <Donation reference={el2} />
      <Newsletter />
      <Footer />
   </div>
  )
}
